﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBodyMovement : MonoBehaviour
{
    //You need to create an input called Directional with q = shift = down/negative and e = space = up/positive
    //float pitch, yaw, roll;
    float xThrow, yThrow, zThrow;
    float mouseXThrow, mouseYThrow;
    [SerializeField] float pitchFactor = 5f;
    [SerializeField] float yawFactor = 5f;
    [SerializeField] float rollFactor = 5f;
    [SerializeField] float translationFactor = 10f;

    float yawThrow;
    float pitchThrow;
    float mouseThrowChangeRate = 0.05f;

    Vector3 originalPos;

    private void Start()
    {
        originalPos = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        ProcessInputs();
        ApplyBodyRotations();
        ApplyBodyTranslations();
    }
    void ProcessInputs()
    {
        xThrow = Input.GetAxis("Horizontal");
        yThrow = Input.GetAxis("Directional");
        zThrow = Input.GetAxis("Vertical");

        mouseXThrow = Input.GetAxis("Mouse X");
        mouseYThrow = Input.GetAxis("Mouse Y");
    }

    void ApplyBodyRotations()
    {
        float pitch = zThrow * pitchFactor - CalculatePitchThrow() * pitchFactor;
        float yaw = CalculateYawThrow() * yawFactor;
        float roll = -xThrow * rollFactor;

        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }
    private float CalculateYawThrow()
    {
        if (mouseXThrow * mouseXThrow <= Mathf.Epsilon)
        {
            if (Mathf.Abs(yawThrow) < 2f * mouseThrowChangeRate)
            {
                yawThrow = 0f;
            }
            else
            {
                yawThrow -= Mathf.Sign(yawThrow) * mouseThrowChangeRate;
            }
        }
        else
        {
            yawThrow = Mathf.Clamp(yawThrow + Mathf.Sign(mouseXThrow) * mouseThrowChangeRate, -1f, 1f);

        }
        return yawThrow;
    }
    private float CalculatePitchThrow()
    {
        if (mouseYThrow * mouseYThrow <= Mathf.Epsilon)
        {
            if (Mathf.Abs(pitchThrow) < 2f * mouseThrowChangeRate)
            {
                pitchThrow = 0f;
            }
            else
            {
                pitchThrow -= Mathf.Sign(pitchThrow) * mouseThrowChangeRate;
            }
        }
        else
        {
            pitchThrow = Mathf.Clamp(pitchThrow + Mathf.Sign(mouseYThrow) * mouseThrowChangeRate, -1f, 1f);

        }
        return pitchThrow;
    }
    void ApplyBodyTranslations()
    {
        Vector3 xOffset = Vector3.right * xThrow;
        Vector3 yOffset = Vector3.up * yThrow;
        Vector3 zOffset = Vector3.forward * zThrow;

        Vector3 posOffset = (xOffset + yOffset + zOffset) * translationFactor * Time.deltaTime;

        transform.localPosition = originalPos + posOffset; //todo - bug transform.right moves the  object along red axis and accounts for rotations but
        //technically hasn't been rotated as it is the parent which has been rotated
    }
}
