﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destruction : MonoBehaviour
{
    public GameObject intact;
    public GameObject broken;
    //public bool isBroken = false;

    bool allowRepair = false;

    private AudioSource audioSource;
    private AudioClip buildingDestroyedSound;

    void Awake()
    {


        intact = transform.GetChild(0).gameObject;
        broken = transform.GetChild(1).gameObject;

        intact.SetActive(true);
        broken.SetActive(false);
    }
    private void Start()
    {
        //adding audio
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.spatialBlend = 0.75f;
        audioSource.volume = 0.9f;
        buildingDestroyedSound = GameController.instance.buildingDestructionSound;
    }

    void Update()
    {
     //   if (Input.GetKeyDown(KeyCode.O))
     //   {
     //       Break();
     //   }

     //   if (Input.GetKeyDown(KeyCode.P))
     //   {
     //       for (int i = 0; i < broken.transform.childCount; i++)
     //       {

     //           broken.transform.GetChild(i).GetComponent<DebrisLerp>().RepairIncrement();
     //       }
     //   }
        //print(broken.active);
    }

    public void Break()
    {
        gameObject.layer = 11;
        intact.SetActive(false);
        broken.SetActive(true);
        allowRepair = true;
        audioSource.PlayOneShot(buildingDestroyedSound);
        //isBroken = true;
    }

    public void Repaired()
    {
        if (allowRepair)
        {
            gameObject.layer = 10;
            intact.SetActive(true);
            broken.SetActive(false);
            //isBroken = false;
            gameObject.GetComponent<Building>().RepairBuilding();
            for (int i = 0; i < broken.transform.childCount; i++)
            {
                print("activavted");
                broken.transform.GetChild(i).GetComponent<DebrisLerp>().WhenRepaired();
            }
            allowRepair = false;
        }
    }

    public void AOERepair()
    {
        if (gameObject.layer == 11)
        {
            for (int i = 0; i < broken.transform.childCount; i++)
            {
                broken.transform.GetChild(i).GetComponent<DebrisLerp>().FullRepair();
            }
        }
    }

    void OnParticleCollision(GameObject other)
    {
        if (gameObject.layer == 11)
        {
            for (int i = 0; i < broken.transform.childCount; i++)
            {
                print("activavted");
                broken.transform.GetChild(i).GetComponent<DebrisLerp>().RepairIncrement();
            }
        }
    }
}
