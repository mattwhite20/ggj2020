﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //You need to create an input called Directional with q = shift = down/negative and e = space = up/positive
    float xThrow, yThrow, zThrow;
    float mouseXThrow, mouseYThrow;
    float pitch, yaw = 0f;
    [SerializeField][Tooltip("deg")] float yawClamp = 75f;
    [SerializeField] [Range(0f, 100f)] float horizontalTranslationSpeed = 5f;
    [SerializeField] [Range(0f, 100f)] float forwardTranslationSpeed = 10f;
    [SerializeField] [Range(0f, 50f)] float rotationSpeed = 50f;

    // Update is called once per frame
    void Update()
    {
        ProcessInputs();
        ApplyTranslations();
        ApplyRotations();
    }
    void ProcessInputs()
    {
        xThrow = Input.GetAxis("Horizontal");
        yThrow = Input.GetAxis("Directional");
        zThrow = Input.GetAxis("Vertical");

        mouseXThrow = Input.GetAxis("Mouse X");
        mouseYThrow = Input.GetAxis("Mouse Y");
    }
    void ApplyTranslations()
    {
        Vector3 xOffset = transform.right * xThrow;
        Vector3 yOffset = transform.up * yThrow;
        Vector3 zOffset = transform.forward * zThrow;

        Vector3 posOffset = ((xOffset + yOffset) * horizontalTranslationSpeed + zOffset * forwardTranslationSpeed) * Time.deltaTime;

        transform.position += posOffset;
    }
    void ApplyRotations()
    {
        pitch += rotationSpeed * mouseXThrow * Time.deltaTime;
        yaw -= rotationSpeed * mouseYThrow * Time.deltaTime;
        yaw = Mathf.Clamp(yaw, -yawClamp, yawClamp);
        //clamp yaw -45 to 45

        transform.eulerAngles = new Vector3(yaw, pitch, 0f);
    }
}
