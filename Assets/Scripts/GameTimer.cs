﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
    int minutes;
    int seconds;

    int maxGameTime;

    Text timerText;
    float timeAtStart;

    bool timerEnabled = false;

    // Start is called before the first frame update
    void Start()
    {
        timerText = GetComponent<Text>();
        timerText.text = ""; //blank
    }
    public void StartTimer()
    {
        maxGameTime = GameController.maxGameTime;
        timeAtStart = Time.time;
        timerEnabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (timerEnabled)
        {
            CalculateTime();
            PrintOutTime();
        }
    }
    void CalculateTime()
    {
        float timeChunk = maxGameTime - Time.time + timeAtStart;
        minutes = (int)Mathf.Floor(timeChunk / 60f);
        seconds = (int)Mathf.Floor(timeChunk - minutes * 60f);

        CheckIfOutOfTime(timeChunk);
    }

    void CheckIfOutOfTime(float timeLeft)
    {
        if (timeLeft < 0f)
        {
            GameController.instance.WinGame();
        }
    }
    void PrintOutTime()
    {
        string secondsString;
        if (seconds < 10)
        {
            secondsString = "0" + seconds;
        } else
        {
            secondsString = "" + seconds;
        } 
        timerText.text = minutes + ":" + secondsString;
    }
}
