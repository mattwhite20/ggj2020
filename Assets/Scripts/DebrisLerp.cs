﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisLerp : MonoBehaviour
{
    private Vector3 startingLocation;
    private Vector3 endingLocation;
    private Quaternion startingRotation;
    private Quaternion endingRotation;

    private Rigidbody rb;

    [SerializeField]
    private float percentageSpeed = 0.05f;
    [SerializeField]
    private float targetPercentage;
    [SerializeField]
    private float percentageIncrement = 0.055f;
    [SerializeField]
    private float percentageComplete;

    private bool ticker = false;

    void Awake()
    {
        endingLocation = transform.position;
        endingRotation = transform.rotation;
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (ticker == true)
        {
            if (targetPercentage > percentageComplete)
            {
                percentageComplete += percentageSpeed;
                transform.position = Vector3.Lerp(startingLocation, endingLocation, percentageComplete);
                transform.rotation = Quaternion.Lerp(startingRotation, endingRotation, percentageComplete);
                if (percentageComplete >= 1f)
                {
                    GetComponentInParent<Destruction>().Repaired();
                }
            }
        }
    }
    public void FullRepair()
    {
        //effect of rocket

        if (!ticker)
        {
            rb.detectCollisions = false;
            rb.isKinematic = true;
            startingLocation = transform.position;
            startingRotation = transform.rotation;
            ticker = true;
        }

        targetPercentage += 1.0f;

        if (targetPercentage > 1f)
        {
            targetPercentage = 1f;
        }
    }
    public void RepairIncrement()
    {

        if (!ticker)
        {
            rb.detectCollisions = false;
            rb.isKinematic = true;
            startingLocation = transform.position;
            startingRotation = transform.rotation;
            ticker = true;
        }

        targetPercentage += percentageIncrement;

        if (targetPercentage > 1f)
        {
            targetPercentage = 1f;
        }
    }

    public void WhenRepaired()
    {
        ticker = false;
        targetPercentage = 0f;
        percentageComplete = 0f;
        rb.detectCollisions = true;
        rb.isKinematic = false;
    }
}
