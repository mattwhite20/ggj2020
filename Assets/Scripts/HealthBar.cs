﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public static HealthBar healthBarScript;
    //todo - needs overall implementation
    Image healthBar;
    int maxHealth;
    int health;
    int deathHealth;

    private void Awake()
    {
        healthBarScript = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        deathHealth = GameController.buildingsLeftDeathAmount;
        //todo - change to serialized drag in maybe
        Invoke("InitialHealthCheck", 1f);//ensureing isn't called before initial building check
        healthBar = GetComponent<Image>();
    }
    private void Update()
    {
     /*   if (Input.GetKeyDown(KeyCode.G))//todo - delete update method
        {
            health -= 1;
            UpdateHealth();
        }
        */
    }

    void InitialHealthCheck()
    {
        maxHealth = Building.activeBuildings.Count - deathHealth;

        health = maxHealth; //- deathHealth;
    }
    public void UpdateHealth()
    {
        health = Building.activeBuildings.Count - deathHealth;
        ChangeHealthBar();
        if (health < 1)
        {
            Invoke("TriggerDeathSequence", 1f);
        }
    }
    void ChangeHealthBar()
    {
        healthBar.GetComponent<Image>().fillAmount = (float) health / maxHealth;
    }
    void TriggerDeathSequence()
    {
        GameController.instance.GameOver();
        //death Sequence
        //todo - Bug, if another building is destroyed, TriggerDeathSequence could be called multiple times
        print("GAME OVER!!!");
    }
}
