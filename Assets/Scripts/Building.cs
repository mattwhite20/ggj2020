﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{

    public static List<Building> activeBuildings = new List<Building>();
    public static List<Building> destroyedBuildings = new List<Building>();

    public bool isDestroyed;

    void Awake()
    {
        activeBuildings.Clear();
        destroyedBuildings.Clear();
    }

    void Start()
    {
        activeBuildings.Add(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DestroyBuilding()
    {
        if (activeBuildings.Contains(this))
        {
            activeBuildings.Remove(this);
            destroyedBuildings.Add(this);

            gameObject.GetComponent<Destruction>().Break();
        }
    }

    public void RepairBuilding()
    {
        if (destroyedBuildings.Contains(this))
        {
            destroyedBuildings.Remove(this);
            activeBuildings.Add(this);
            HealthBar.healthBarScript.UpdateHealth();
            //myMaterial.material.SetColor("_Color", Color.green);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "badguy")
        {
            DestroyBuilding();
            HealthBar.healthBarScript.UpdateHealth();
        }
    }    
}
