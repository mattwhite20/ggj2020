﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionFX : MonoBehaviour
{
    [SerializeField] float radius = 45f;
    AudioSource audioSource;
    [SerializeField] AudioClip explosionSound;
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(explosionSound);
        Destroy(gameObject, 6f);
        AreaRepair();
    }
    void AreaRepair()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);
        int i = 0;
        while (i < hitColliders.Length)
        {
            print("called");
            if (hitColliders[i].gameObject.GetComponent<Destruction>() != null)
            {
                hitColliders[i].gameObject.GetComponent<Destruction>().AOERepair();
            }
            i++;
        }
    }
}
