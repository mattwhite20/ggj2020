﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpTest : MonoBehaviour
{
    public Vector3 startLocation;
    public Vector3 endLocation;

    private void FixedUpdate()
    {
        transform.position = Vector3.Lerp(startLocation, endLocation, 0.4f);
    }
}
