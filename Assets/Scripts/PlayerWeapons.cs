﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapons : MonoBehaviour
{
    [SerializeField] ParticleSystem bulletParticles;
    [SerializeField] GameObject missleObject;
    [SerializeField] Transform missleDirectionObject;
    [SerializeField] float rocketCoolDown = 2f; //just for testing purposes - I think like 30sec would be good
    [SerializeField] AudioSource laserAudioSource;
    [SerializeField] AudioSource rocketAudioSource;
    [SerializeField] AudioClip blastOffRocket;
    float rocketShotTime = 0f;
    bool rocketReady = false;
    bool laserTicker = false;
    float rocketReadyTime;

    private void Start()
    {
        rocketReadyTime = Time.time + rocketCoolDown;
    }

    // Update is called once per frame
    void Update()
    {
        ReceiveInputs();
    }
    void ReceiveInputs()
    {
        if (Input.GetButton("Fire1"))
        {
            EnableBullets(true);
            if (!laserTicker)
            {
                laserTicker = true;
                laserAudioSource.Play();
            }
        } else
        {
            EnableBullets(false);
            laserAudioSource.Stop();
            laserTicker = false;
        }
        if (Input.GetButtonDown("Fire2") && rocketReady)
        {
            ShootMissle();
            rocketAudioSource.PlayOneShot(blastOffRocket);
        } 
    }
    void EnableBullets(bool isActive)
    {
        var bulletEmission = bulletParticles.emission;
        bulletEmission.enabled = isActive;
    }
    void ShootMissle()
    {
        rocketReadyTime = Time.time + rocketCoolDown;

        rocketShotTime = Time.time;

        rocketReady = false;

        GameObject missle = Instantiate(missleObject, missleDirectionObject.position, transform.rotation, null) as GameObject;

        Vector3 missleDirection = (missleDirectionObject.position - transform.position).normalized;

        missle.GetComponent<Missle>().AquireTarget(missleDirection);
    }
    public float GetRocketReadyPercentage()
    {
        if (!rocketReady)
        {
            float rocketPercentage = (Time.time-rocketShotTime) / rocketCoolDown;
            if (rocketPercentage > 1.0f)
            {
                rocketReady = true;
                rocketPercentage = 1.0f;
            }
            return rocketPercentage;
        } else
        {
            return 1.0f;
        }
    }
}
