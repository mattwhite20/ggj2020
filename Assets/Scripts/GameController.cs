﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    public static int maxGameTime = 120;
    public static int buildingsLeftDeathAmount = 65;//number of buildings left that will trigger player death
    public GameObject titleScreen;
    public GameObject gameUI;
    public GameTimer timer;
    public GameObject player;
    public PlayerMovement playerMovementScript;
    public GameObject enemy;
    public GameObject fadeScreen;
    public AudioClip buildingDestructionSound;
    public GameObject timerBackground;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        titleScreen.SetActive(true);
        enemy.SetActive(false);
        player.SetActive(false);
        playerMovementScript.enabled = false;
        gameUI.SetActive(false);
        timerBackground.SetActive(false);
    }

    public void StartGame()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        titleScreen.SetActive(false);
        enemy.SetActive(true);
        player.SetActive(true);
        playerMovementScript.enabled = true;
        gameUI.SetActive(true);
        timer.StartTimer();
        timerBackground.SetActive(true);
    }

    public void WinGame()
    {
        fadeScreen.SetActive(true);//for now - make individual screen
        fadeScreen.GetComponent<DeathScreen>().SceneToLoad(2);
    }
    public void GameOver()
    {
        fadeScreen.SetActive(true);
        fadeScreen.GetComponent<DeathScreen>().SceneToLoad(1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
