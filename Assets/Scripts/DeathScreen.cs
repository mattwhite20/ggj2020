﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeathScreen : MonoBehaviour
{
    Image image;
    Color alphaStep = new Color(0f, 0f, 0f, 0.01f);
    int sceneToLoad;
    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();

    }
    public void SceneToLoad(int loadScene)
    {
        sceneToLoad = loadScene;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateAlpha();
    }
    void UpdateAlpha()
    {
        image.color += alphaStep;
        if (image.color.a > 0.98f)
        {
            LoadGameOverScene();
        }
    }
    void LoadGameOverScene()
    {
        SceneManager.LoadScene(sceneToLoad);
    }
}
