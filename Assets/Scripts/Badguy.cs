﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Badguy : MonoBehaviour
{
    NavMeshAgent myNMA;

    //jumping
    public float jumpDistance;
    public float jumpTime;

    Vector3 jumpStartPos;
    Vector3 jumpTarget;
    float jumpStartTime;
    bool isJumping;

    public Animator robotAnimator;
    public AudioSource jumpSound;
    public AudioClip landingSound;

    public float targetSwapDelay;
    float targetSwapTimer;

    // Start is called before the first frame update
    void Start()
    {
        isJumping = false;
        myNMA = GetComponent<NavMeshAgent>();
        targetSwapTimer = targetSwapDelay;
    }

    // Update is called once per frame
    void Update()
    {
        targetSwapTimer += Time.deltaTime;

        if (targetSwapTimer >= targetSwapDelay)
        {
            if (myNMA.enabled && !myNMA.hasPath && Building.activeBuildings.Count > 0)
            {
                ChooseTarget();
                targetSwapTimer = 0;
            }
        }

        if (isJumping)
        {
            StartCoroutine(JumpAnimation("isJumping"));
            Jump();
        }
    }

    void ChooseTarget()
    {
        int ranNum = Random.Range(0, Building.activeBuildings.Count);

        if (Vector3.Distance(transform.position, Building.activeBuildings[ranNum].transform.position) > jumpDistance)
        {
            //jump
            transform.LookAt(new Vector3(Building.activeBuildings[ranNum].transform.position.x, transform.position.y, Building.activeBuildings[ranNum].transform.position.z));
            jumpSound.Play();
            myNMA.enabled = false;
            isJumping = true;
            jumpStartTime = Time.time;
            jumpStartPos = transform.position;
            jumpTarget = Building.activeBuildings[ranNum].transform.position;
            jumpTarget += new Vector3(0, transform.localScale.y, 0);
        }

        else
        {
            //walk
            myNMA.SetDestination(Building.activeBuildings[ranNum].transform.position);
        }
    }

    void Jump()
    {
        //lerp to jump point
        Vector3 center = (jumpStartPos + jumpTarget) * 0.5f;
        center -= new Vector3(0, 1, 0);

        Vector3 myRelativeCenter = jumpStartPos - center;
        Vector3 targetRelativeCenter = jumpTarget - center;

        float fractionComplete = (Time.time - jumpStartTime) / jumpTime;

        transform.position = Vector3.Slerp(myRelativeCenter, targetRelativeCenter, fractionComplete) + center;
        //transform.position += center;

        if (fractionComplete >= 1f)
        {
            myNMA.enabled = true;
            isJumping = false;
            targetSwapTimer = 0;
            jumpSound.PlayOneShot(landingSound);
        }
    }

    public IEnumerator JumpAnimation (string paramname)
    {
        robotAnimator.SetBool(paramname, true);
        yield return null;
        robotAnimator.SetBool(paramname, false);
    }
}
