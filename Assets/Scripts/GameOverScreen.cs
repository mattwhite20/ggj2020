﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverScreen : MonoBehaviour
{
    public float gameOverWaitTime = 4f;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("LoadFirstScene", gameOverWaitTime);
    }
    void LoadFirstScene()
    {
        SceneManager.LoadScene(0);
    }
}
