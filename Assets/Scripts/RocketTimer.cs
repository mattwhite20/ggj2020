﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RocketTimer : MonoBehaviour
{
    [SerializeField] PlayerWeapons playerWeapons;

    Image image;

    private void Start()
    {
        image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        image.fillAmount = playerWeapons.GetRocketReadyPercentage();
    }
}
