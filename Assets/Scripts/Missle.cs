﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missle : MonoBehaviour
{
    Vector3 directionVector;

    [SerializeField] float missleSpeed = 50f;

    [SerializeField] GameObject explosionFX;

    // Update is called once per frame
    void Update()
    {
        SeekTarget();
    }

    public void AquireTarget(Vector3 missleDirection)
    {
        directionVector = missleDirection;
    }
    void SeekTarget()
    {
        Vector3 posOffset = missleSpeed * Time.deltaTime * directionVector;
        transform.position += posOffset;
        Invoke("Explode",30f);
    }
    private void OnCollisionEnter(Collision collision)
    {
        Explode();
    }
    void Explode()
    {
        Instantiate(explosionFX, transform.position, Quaternion.identity, null);
        Destroy(gameObject);
    }
}
